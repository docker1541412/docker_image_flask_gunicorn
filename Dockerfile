# Первый этап: сборка
FROM python:3.8-alpine AS builder

# Обновляем apk и устанавливаем зависимости, нужные только на этапе сборки
RUN apk add --no-cache \
    postgresql-dev \
    gcc \
    musl-dev \
    libffi-dev

# Создаем рабочую директорию
WORKDIR /app

# Копируем файлы requirements-server.txt в контейнер
COPY requirements-server.txt .
COPY requirements.txt .

# Устанавливаем зависимости из requirements-server.txt
RUN pip install --no-cache-dir -r requirements-server.txt
RUN pip install --no-cache-dir -r requirements.txt

# Копируем все файлы приложения в контейнер
COPY . .

# Инициализация и установка тестовых данных в базу данных (если требуется)
RUN flask db upgrade
RUN python seed.py

# Второй этап: финальный образ
FROM python:3.8-alpine

# Устанавливаем необходимые зависимости для запуска
RUN apk add --no-cache \
    postgresql-libs

# Создаем рабочую директорию
WORKDIR /app

# Копируем установленные зависимости из этапа сборки
COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=builder /usr/local/bin /usr/local/bin

# Копируем файлы вашего проекта
COPY --from=builder /app /app

# Expose порт, на котором будет работать ваше приложение Flask с Gunicorn (8000)
EXPOSE 8000

# Указываем команду по умолчанию для запуска приложения с Gunicorn
CMD ["gunicorn", "app:app", "-b", "0.0.0.0:8000"]

